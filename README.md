Object-oriented [healpy](https://healpy.readthedocs.io) wrapper with support for multi-resolution maps (a.k.a. [multi-order coverage](http://ivoa.net/documents/MOC) maps).

For full documentation visit: https://mhealpy.readthedocs.io/