.. mhealpy documentation master file, created by
   sphinx-quickstart on Mon Sep 21 09:50:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mhealpy's documentation!
======================================

`HEALPix <https://healpix.jpl.nasa.gov>`_ is a **H**\ ierarchical, **E**\ qual **A**\ rea, and iso-**L**\ atitude **Pix**\ elisation of the sphere. It has been implemented in multiple languages, including Python through the `healpy <https://healpy.readthedocs.io>`_ library.

`mhealpy` is an object-oriented wrapper of `healpy`, in the fashion of `Healpix C++ <https://healpix.sourceforge.io/html/Healpix_cxx/index.html>`_, that extends its functionalities to handle multi-resolution maps.

Reference: Martinez-Castellanos, I. et al. *Multi-Resolution HEALPix Maps for Multi-Wavelength and Multi-Messenger Astronomy*. `arXiv: 2111.11240 [astro-ph.IM] <https://arxiv.org/abs/2111.11240>`_ (2021).

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   tutorials/Intro.ipynb
   tutorials/index
   api/index
   
