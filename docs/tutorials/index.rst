Examples
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   LIGO_IO_Resampling.ipynb
   IPN_annulus.ipynb
