
import numpy as np
from numpy import array_equal as arr_eq
from numpy.lib import recfunctions as recfun

from pytest import approx

from astropy.visualization.wcsaxes import WCSAxes

import mhealpy as mhp
from mhealpy import HealpixMap

from copy import deepcopy

import healpy as hp

import matplotlib
matplotlib.use('agg')

def test_operations():

    m1 = HealpixMap(range(-6,6), scheme = 'ring')
    m2 = HealpixMap(range(12*4), scheme = 'nested')
    m3 = HealpixMap([ 0. ,  1. ,  2. ,  3. ,  4. ,  5. ,  6. ,  7. ,  8. ,  9. ,  2.5, 2.5,  2.5,  2.5, 11. ],
                    [   4,    5,    6,    7,    8,    9,   10,   11,   12,   13,   56,  57,   58,   59,   15])
    m4 = HealpixMap([ 0.  ,  0.25,  0.25,  0.25,  0.25,  2.  ,  3.  ,  4.  ,  5.  ,  6.  ,  7.  ,  8.  ,  9.  , 10.  , 11.  ],
                    [    4,    20,    21,    22,    23,     6,     7,     8,     9,    10,    11,    12,    13,    14,    15])
    m5 = HealpixMap(range(12*4), scheme = 'ring')
    m6 = HealpixMap(4*np.ones(12), scheme = 'nested')
    m7 = HealpixMap(4*np.ones(12), scheme = 'nested', density = True)
    m8 = HealpixMap(4*np.ones(12), scheme = 'ring')
    m9 = HealpixMap(4*np.ones(12), scheme = 'ring', density = True)
    m10 = HealpixMap(np.ones(12*4), scheme = 'nested')
    m11 = HealpixMap(np.ones(12*4), scheme = 'nested', density = True)
    m12 = HealpixMap([4., 4., 4., 4., 3., 3., 3., 2., 2., 2., 1., 1., 1., 0., 0., 0., 0.,
                      0., 0., 0., 0., 0., 0., 0.],
                     [1024, 1025, 1026, 1027,  257,  258,  259,   65,   66,   67,   17,  18,   19,    5,    6,    7,    8,    9,   10,   11,   12,   13,   14,   15],
                     density=True)
    m13 = HealpixMap([3., 3., 3., 3., 2., 2., 2., 1., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                     [316, 317, 318, 319,  76,  77,  78,  16,  17,  18,   5,   6,   7, 8,   9,  10,  11,  12,  13,  14,  15],
                     density=True)
    
    # By scalar
    assert m1*3 == HealpixMap(np.array(m1)*3, scheme = 'ring')
    assert m1/3 == HealpixMap(np.array(m1)/3, scheme = 'ring')
    assert m1//3 == HealpixMap(np.array(m1)//3, scheme = 'ring')
    assert m1+3 == HealpixMap(np.array(m1)+3, scheme = 'ring')
    assert m1-3 == HealpixMap(np.array(m1)-3, scheme = 'ring')
    assert m1**3 == HealpixMap(np.array(m1)**3, scheme = 'ring')

    assert m3*3 == HealpixMap(3*m3.data, m3.uniq)

    # Right operations
    assert 3*m1 == HealpixMap(3*np.array(m1), scheme = 'ring')
    assert 3/m1 == HealpixMap(3/np.array(m1), scheme = 'ring')
    assert 3//m1 == HealpixMap(3//np.array(m1), scheme = 'ring')
    assert 3+m1 == HealpixMap(3+np.array(m1), scheme = 'ring')
    assert 3-m1 == HealpixMap(3-np.array(m1), scheme = 'ring')

    assert 3*m3 == HealpixMap(m3.data*3, m3.uniq)

    # Unitary operations
    assert abs(m1) == HealpixMap(abs(np.array(m1)), scheme = 'ring')
    assert -m1 == HealpixMap(-np.array(m1), scheme = 'ring')
    
    # Same grid
    assert m1*m1 == HealpixMap(np.array(m1)**2, scheme = 'ring')
    assert m2*m2 == HealpixMap(np.array(m2)**2, scheme = 'nested')

    assert m3*m3 == HealpixMap(m3.data*m3.data, m3.uniq)

    # Single resolution, same order, different scheme
    m = m5*m2
    assert m.scheme == m5.scheme
    assert m[0] == 0*3 and m[1] == 1*7 and m[2] == 2*11 and m[3] == 3*15 
    assert m[30] == 30*20 and m[31] == 31*39 and m[32] == 32*24 and m[33] == 33*43 

    m = m2*m5
    assert m.scheme == m2.scheme
    assert m[0] == 0*13 and m[1] == 1*5 and m[2] == 2*4 and m[3] == 3*0 
    assert m[30] == 30*25 and m[31] == 31*18 and m[32] == 32*44 and m[33] == 33*37 

    # Same scheme different nside
    # ---Nested
    assert m2*m6 == m2 # m6 is histogram-like
    assert m2*m7 == 4*m2 # m7 is a density map

    # ---Ring
    assert m5*m8 == m5 # m8 is histogram-like
    assert m5*m9 == 4*m5 # m9 is a density map

    # Single resolution, different nside and scheme
    assert m5*m6 == m5 # m6 is histogram-like
    assert m5*m7 == 4*m5 # m7 is a density map

    assert m2*m8 == m2 # m8 is histogram-like
    assert m2*m9 == 4*m2 # m9 is a density map

    # Forcing to downgrade with imul
    # -- Nested
    m6copy = deepcopy(m6) # m6 is histogram-like
    m6copy *= m10 # m10 is histogram
    assert m6copy == 4*m6

    m7copy = deepcopy(m7) # m7 is a density map
    m7copy /= m10 # m10 is histogram
    assert m7copy.density() == m7copy.density()
    m7copy.density(True, False)
    assert m7copy == m7/4

    m7copy = deepcopy(m7) # m7 is a density map
    m7copy //= m10 # m10 is histogram
    assert m7copy.density() == m7copy.density()
    m7copy.density(True, False)
    assert m7copy == m7//4

    m7copy = deepcopy(m7) # m7 is a density map
    m7copy **= m10 # m10 is histogram
    assert m7copy.density() == m7copy.density()
    m7copy.density(True, False)
    assert m7copy == m7**4

    # -- Ring
    m8copy = deepcopy(m8) # m8 is histogram-like
    m8copy -= m11 # m10 is density
    assert m8copy == m8 - 1

    m9copy = deepcopy(m9) # m9 is a density map
    m9copy += m11 # m10 is density
    assert m9copy.density() == True
    m9copy.density(True, False)
    assert m9copy == m9 + 1
    

    # MOC with MOC
    m12timesm13 = HealpixMap([4., 4., 4., 4., 3., 3., 3., 2., 2., 2., 1., 1., 2., 2., 2., 3., 3.,
                              3., 3., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                             [1024, 1025, 1026, 1027,  257,  258,  259,   65,   66,   67,   17,
                              18,   76,   77,   78,  316,  317,  318,  319,    5,    6,    7,
                              8,    9,   10,   11,   12,   13,   14,   15],
                             density=True)
    
    assert m12*m13 == m12timesm13

def test_read_write(tmp_path):

    # Simple read and write
    contents = [2,   2, 0,0,0,0,0, 0, 0,  1,  0, 0, 0, 0,  2,  2,  1,  1]
    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14, 15]
    m = HealpixMap(contents, uniq)
    m.moc_sort()
    
    m.write_map(tmp_path/"map.fits")
    
    mr = HealpixMap.read_map(tmp_path/"map.fits")
    mr.moc_sort()

    assert m == mr

    # Multiple colums
    m2 = deepcopy(m)
    m.write_map(tmp_path/"map.fits", extra_maps = m2, overwrite = True)

    mr = HealpixMap.read_map(tmp_path/"map.fits", field = 2)
    mr.moc_sort()
    
    assert m == mr
    
def test_adaptive_mesh():

    # From pixels
    max_nside = 4
    hires_pix = [0,5]
    m = HealpixMap.moc_from_pixels(4, [0,5], nest = False)

    uniq = [16, 17, 18, 76, 77, 78, 79, 5,6, 7, 8,9,10,11,12,13,14,15]
    contents = np.zeros(len(uniq))
    mGood = HealpixMap(contents, uniq)

    m.moc_sort()
    mGood.moc_sort()
    
    assert m == mGood

    # Using adaptive_moc_mesh
    def split_fun(start, stop):
        
        for pix in hp.ring2nest(max_nside, hires_pix):
            if start <= pix and pix < stop:
                return True

        return False

    m2 = HealpixMap.adaptive_moc_mesh(max_nside, split_fun)
    m2.moc_sort()
    
    assert m2 == mGood

def test_moc_histogram():
    
    m = HealpixMap.moc_histogram(4, [0, 5, 50], 1, weights = [1, 2, 3], nest=True)
    
    uniq =     [ 19,  18,  71,  70,  69,  68,  16,   5,   6,  31,  30,  29, 115,
                 114, 113, 112,   8,   9,  10,  11,  12,  13,  14,  15]
    contents = [0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0]
    mGood = HealpixMap(contents, uniq)

    m.moc_sort()
    mGood.moc_sort()
    
    assert m == mGood

def test_sort():

    uniq  = [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14, 15]
    contents = np.arange(len(uniq))
    mGood = HealpixMap(contents, uniq)

    uniq  =    [5, 17, 18, 19, 15, 6, 7, 14, 64, 65, 66,  8,  9, 10, 11, 12, 13, 67]
    contents = [7,  4,  5,  6, 17, 8, 9, 16,  0,  1,  2, 10, 11, 12, 13, 14, 15,  3]

    m = HealpixMap(contents, uniq)

    assert m != mGood

    m.moc_sort()
    assert m == mGood

def test_plot():

    # This only tests that there are no exeception
    # generating the plot, not that the plots
    # was drawn correctly

    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15]
    contents = np.arange(len(uniq))
    m = HealpixMap(contents, uniq)

    m.plot()
    m.plot('mollview')
    m.plot('orthview')
    m.plot('cartview')
    plot, ax = m.plot(ax = 'orthview', alpha = 0.5)

    assert isinstance(plot, matplotlib.image.AxesImage)
    assert isinstance(ax, WCSAxes)

    m.plot(ax)
    
def test_interp():

    # MOC
    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15]
    contents = np.arange(len(uniq))
    m = HealpixMap(contents, uniq)

    theta = np.linspace(0, np.pi, 10)
    phi = np.linspace(0, 2*np.pi, 10)

    mEq = m.rasterize(m.nside, scheme = 'ring')
    
    for t,p in zip(theta, phi):
        good_val = hp.get_interp_val(np.array(mEq), t, p)
        assert m.get_interp_val(t, p) == approx(good_val)

        lon,lat = np.rad2deg(p), 90-np.rad2deg(t)
        good_val = hp.get_interp_val(np.array(mEq), lon, lat, lonlat = True)
        assert m.get_interp_val(lon, lat, lonlat = True) == approx(good_val)
        
